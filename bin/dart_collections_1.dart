void main(List<String> arguments) {
// 1) Дан массив с числами [1, 7, 12, 3, 56, 2, 87, 34, 54]. Выведите в конслоли
// первый, пятый и последний элемент списка.

  List<int> list1 = [1, 7, 12, 3, 56, 2, 87, 34, 54];
  print('Н А Ч А Л О ');

  print('${list1[0]}, ${list1[4]}, ${list1.last} ');
  print('конец   задания');

// 2) Даны два массива с числами [3, 12, 43, 1, 25, 6, 5, 7] и [55, 11, 23, 56,
// 78, 1, 9]. Объедените данные массивы и выведите в консоли,

  List listFirst = [3, 12, 43, 1, 25, 6, 5, 7];
  List listSecond = [55, 11, 23, 56, 78, 1, 9];
  listFirst.addAll(listSecond);
  print(listFirst);
  print('конец   задания');

//3) Дан массив ['a','d','F','l','u','t','t','e','R','y','3','b','h','j'],
//из данного массива необходимо вывести в консоли массив ['F','l','u','t','t','e','R']

// final colors = <String>['red', 'green', 'blue', 'orange', 'pink'];
// print(colors.sublist(1, 3)); // [green, blue]

  List flutter = [
    'a',
    'd',
    'F',
    'l',
    'u',
    't',
    't',
    'e',
    'R',
    'y',
    '3',
    'b',
    'h',
    'j'
  ];
  print(flutter.sublist(2, 9)); // вариант 1
  // вариант 2   print(flutter.getRange(2, 9));
  print('конец   задания');

  // 4) выведите true если массивы слодержит цифру 3 [1, 2, 3, 4, 5, 6, 7],
  //также покажите первый и последний элемент массива и его длину

  List listFour = [1, 2, 3, 4, 5, 6, 7];

  print(listFour.first);
  print(listFour.last);
  print(listFour.length);
  print(listFour.contains(3));
  print('конец   задания');

// У вас есть список со значениями = [601 123, 2, "dart", 45, 95, "dart24", 1];
// Попробуйте определить содержит ли список такие элементы как: 1 – ‘dart’; 2 – 951;
// Вы должны получить принты для задач:
// 1 – true
// 2 – false

  List listDartTrue = [601123, 2, "dart", 45, 95, "dart24", 1];
  print(listDartTrue.contains('dart'));
  print(listDartTrue.contains(951));
  print('конец   задания');

// У вас есть список со значениями = [‘post’, 1, 0, ‘flutter’];
// И переменная String myDart = ‘Flutter’; Попробуйте определить содержит ли
// список значение переменной myDart (Именно с большой буквы). Вы должны получить
// print со значением true;

  List listContainsFlutter = ['post', 1, 0, 'flutter'];

  String myDart = 'Flutter';
  print(listContainsFlutter.contains(myDart.toLowerCase()));
  // вариант 2
  // listContainsFlutter.last = 'Flutter'; в массиве последний элемент меняю на нужный формат текста
  // print(listContainsFlutter.contains(myDart));
  print('К О Н Е Ц      З А Д А Н И Я');

// У вас есть список со значениями [“I”, “Started", "Learn", "Flutter", "Since", "April"];
// И переменная String myFlutter = ‘’; Задача: Объединить все элементы массива
// в одну строку и разделить каждое  слово символом ‘*’ и сделать принт перменной
//  myFlutter;  Print(myFlutter); В консоли вы должны получить:
// I * Started * Learn * Flutter * Since * April

  List listAddStar = ['I', 'Started', 'Learn', 'Flutter', 'Since', 'April'];
  String myFlutter = '';
  print(myFlutter = listAddStar.join(' * ').toString());
  print(myFlutter.runtimeType);

  // listAddStar.forEach((i) {
  //   print(i + '*');
  // });  не то, что нужно
  // myFlutter.forEach((i) {
  //   print(i + '*');
  // }); не то, что нужно
  print('конец   задания');

// У вас есть массив со значением = [1, 9, 3, 195, 202, 2, 5, 7, 9, 10, 3, 15, 0, 11];
// Ваша задача провести сортировку массива, чтобы все элементы начинались по
// возрастающей. Вы должны получить в консоли: [0, 1, 2, 3, 3, 5, 7, 9, 9, 10, 11, 15, 195, 202]

  List listMySort = [1, 9, 3, 195, 202, 2, 5, 7, 9, 10, 3, 15, 0, 11];
  listMySort.sort((a, b) => a.compareTo(b));
  // listMySort.sort();
  print(listMySort);

  print('конец   задания');
  // print('  выявление дубликатов самостоятельно поюзать ');

  // выявление дубликата в List-е сложной конструкции
  // List listDuplicate = [
  //   {"name": 'John', 'age': 18},
  //   {"name": 'Jane', 'age': 21},
  //   {"name": 'Mary', 'age': 23},
  //   {"name": 'Mary', 'age': 27},
  // ];

  // List names = [];
  // listDuplicate.forEach((u) {
  //   if (names.contains(u["name"]))
  //     print("duplicate ${u["name"]}");
  //   else
  //     names.add(u["name"]);
  // });

  // выявление дубликата в стандартном List-е
  // Н Е   Р А Б О Т А Е Т
//   List listSimpleDuplicate = ['Misha', 'Vanya', 56, 48, 56, 'misha', 'Vanya'];
//   listSimpleDuplicate.forEach((d) {
//     if (listSimpleDuplicate.contains(d[''])) {
//       print('duplicateSimple $d');
//     } else {
//       listSimpleDuplicate.add(d);
//     }
//   });

// // получение уникального списка без дубликатов
// // обратите внимание, что префикс букв воспринимается как разное
//  а как получить дубликаты?

  // List<String> countries = [
  //   "Nepal",
  //   "Nepal",
  //   "USA",
  //   "Canada",
  //   "Canada",
  //   "China",
  //   "china",
  //   "Russia",
  // ];

  // var seen = Set<String>();
  // List<String> uniquelist =
  //     countries.where((country) => seen.add(country)).toList();
  // print(uniquelist);

// получение уникального списка без дубликатов КОРОТКАЯ ЗАПИСЬ
//   List nll = [1, 5, 6, 4, 7, 8, 2, 1, 5];
// //duplicate numbers
//   var nnn = nll.toSet().toList();
//   print(nnn);
}
